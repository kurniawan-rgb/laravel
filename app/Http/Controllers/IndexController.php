<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home(){
        return view('tampilan.home');
    }

    public function tabel(){
        return view('tampilan.dataTabel');
    }
}

