<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('tampilan.form');
    }

    public function welcome(Request $request){
        $nama = $request ['FirstName'];
        $belakang = $request ['LastName'];

        return view('tampilan.welcome' , compact('nama','belakang'));
    }
}
