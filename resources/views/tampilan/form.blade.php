@extends('master.master')
@section('tittle')
Halaman Daftar
@endsection
@section('content')
<h1>Buat Account Baru</h1>
<h4>Sign Up Form</h4>
<form action="/welcome" method="post">
    @csrf
    <label for="firstname">First Name :</label><br><br>
    <input type="text" name="FirstName" id="firstname"><br><br>
    <label for="lasttname">Last Name :</label><br><br>
    <input type="text" name="LastName" id="lastname"><br><br>
    <label for="Gender">Gender :</label><br><br>
    <input type="radio" name="Male" id="Gender">Male<br>
    <input type="radio" name="Female" id="Gender">Female<br><br>
    <label for="Nationality"> Nationality </label> <br> <br>
    <select name="Nationality" id="negara">
        <option value="Indonesia"> Indonesia </option>
        <option value="Malaysia">Malaysia</option>
        <option value="Dubai">Dubai</option>
        <option value="Other">Other</option>
    </select><br><br>
    <label for="Bahasa">Languange Spoke</label><br><br>
    <input type="checkbox" name="Bahasa Indonesia" id="Bahasa"> Bahasa Indonesia <br>
    <input type="checkbox" name="English" id="Bahasa"> English<br>
    <input type="checkbox" name="Bahasa Indonesia" id="Bahasa"> Other<br><br>
    <label for="Bio">Bio</label><br><br>
    <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection